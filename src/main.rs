#[macro_use]
extern crate clap;
extern crate gmarkov_lib;

use gmarkov_lib::MarkovChain;
use std::fs::File;
use std::io::{self, BufRead};

fn real_main() -> Result<(), String> {
    let matches = clap_app!(gmarkov =>
      (version: "1.0.0")
      (author: "Graham Scheaffer <gischeaffer@gmail.com>")
      (about: "Generates random text based on the input")
      (@arg ORDER: -o --order +takes_value "Order of the markov chain (default 1)")
      (@arg LINES: -n --lines +takes_value "Number of lines to output (default 1)")
      (@arg INPUT: +required "File to take input from")
    )
    .get_matches();

    let order = matches
        .value_of("ORDER")
        .unwrap_or("1")
        .parse()
        .map_err(|e| format!("Failed to parse order option: {}", e))?;
    let mut chain = MarkovChain::with_order::<char>(order);

    let file_name = matches.value_of("INPUT").unwrap();

    let reader: Box<dyn BufRead> = if file_name == "-" {
        Box::new(io::BufReader::new(io::stdin()))
    } else {
        Box::new(io::BufReader::new(
            File::open(file_name).map_err(|_| "Failed to open file".to_string())?,
        ))
    };

    for line in reader.lines() {
        chain.feed(
            line.map_err(|_| "Failed to read file".to_string())?
                .trim()
                .chars(),
        );
    }

    let lines = matches
        .value_of("LINES")
        .unwrap_or("1")
        .parse()
        .map_err(|e| format!("Failed to parse lines option: {}", e))?;

    for _ in 0..lines {
        println!("{}", chain.clone().into_iter().collect::<String>());
    }

    Ok(())
}

fn main() {
    let res = real_main();

    if let Err(error) = res {
        eprintln!("{}", error);
        std::process::exit(1);
    }
}
